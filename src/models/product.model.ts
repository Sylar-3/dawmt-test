import { ProductEntity, ProductListEntity } from '../entities/product.entity';

export class ProductModel {
  id!: number;
  name!: string;
  price!: number;
  description!: string;
  category!: string;
  img!: string;
  rating!: {
    rate: number;
    count: number;
  };

  constructor(productEntity?: ProductEntity) {
    if (productEntity) {
      this.id = productEntity.id;
      this.name = productEntity.title;
      this.price = productEntity.price;
      this.description = productEntity.description;
      this.category = productEntity.category;
      this.img = productEntity.image;
      this.rating = productEntity.rating;
    }
  }

  public mapToArray(productsList: ProductListEntity): Array<ProductModel> {
    return productsList.map((product) => new ProductModel(product));
  }
}
