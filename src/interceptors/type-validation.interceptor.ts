/* eslint-disable @typescript-eslint/no-explicit-any */
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { VerifyEntityService } from '../app/shared/services/verify-entity.service';
import { environment } from '../environments/environment';


@Injectable()
export class TypeValidationInterceptor implements HttpInterceptor {
  constructor(private readonly _verifyEntityService: VerifyEntityService) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      tap((httpResponseEvent: HttpEvent<any>) => {
        if (httpResponseEvent instanceof HttpResponse) {
          let covered: boolean = false;
          //* Please add new apis here to validate and create new entities
          const coveredApis: Array<string> = [
            '/products'
            ];

          //* Check if covered in service
          coveredApis.map((api: string) => {
            if (request.url.includes(api)) {
              covered = true;
            }
          });

          if (!environment.production && !covered) {
            this._verifyEntityService.emitEvent(
              `You forgot to add your new api to validate in [type-validation.interceptor.ts] 💪
               API: ${request.url}`,
            );
            // eslint-disable-next-line no-console
            console.warn(`Please validate API: ${request.url}`);
          }
        }
      }),
    );
  }
}
