import { Routes } from '@angular/router';
import { ContentComponent } from './pages/content.component';

export const routes: Routes = [
  {
    path: 'home',
    component: ContentComponent,
  },
  {
    path: '**',
    redirectTo: 'home',
    pathMatch: 'full',
  },
];
