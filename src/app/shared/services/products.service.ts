import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { VerifyEntityService } from './verify-entity.service';
import { Observable, map, tap } from 'rxjs';
import { ProductModel } from '../../../models/product.model';
import { ProductListEntity } from '../../../entities/product.entity';
import { environment } from '../../../environments/environment';
import { CategoriesEntity } from '../../../entities/categories.entity';

@Injectable()
export class ProductsService {
  private readonly PRODUCT_ENDPOINTS = {
    product: '/products',
    categories: '/products/categories',
  };

  constructor(
    private readonly _http: HttpClient,
    private readonly _verifyEntityService: VerifyEntityService,
  ) {}

  public getProductsList(
    category: string | null,
  ): Observable<Array<ProductModel>> {
    return this._http
      .get<ProductListEntity>(
        `${environment.CURRENT_DOMAIN}${this.PRODUCT_ENDPOINTS.product}${category ? '/category/' + category : ''}`,
      )
      .pipe(
        tap((res: ProductListEntity) =>
          this._verifyEntityService.type(
            this.PRODUCT_ENDPOINTS.product,
            ProductListEntity,
            res,
          ),
        ),
        map((products: ProductListEntity) =>
          new ProductModel().mapToArray(products),
        ),
      );
  }

  public getCategoriesList(): Observable<Array<string>> {
    return this._http
      .get<CategoriesEntity>(
        `${environment.CURRENT_DOMAIN}${this.PRODUCT_ENDPOINTS.categories}`,
      )
      .pipe(
        tap((res: CategoriesEntity) =>
          this._verifyEntityService.type(
            this.PRODUCT_ENDPOINTS.categories,
            CategoriesEntity,
            res,
          ),
        ),
        map((products: CategoriesEntity) => products.map((product) => product)),
      );
  }
}
