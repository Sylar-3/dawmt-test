/* eslint-disable @typescript-eslint/typedef */
import { z } from 'zod';

import { EventEmitter, Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';


@Injectable({ providedIn: 'root' })
export class VerifyEntityService {
  emittedZodEvent: EventEmitter<string> = new EventEmitter<string>();

  emitEvent(content: string): void {
    this.emittedZodEvent.emit(content);
  }

  type<T extends z.ZodTypeAny>(
    apiName: string,
    zodObj: T,
    data: unknown,
  ): void {
    const result = zodObj.safeParse(data);
    if (!result.success) {
      const fieldErrors = result.error.errors;

      if (!environment.production) {
        this.emitEvent(`API: ${apiName}`);
      }

      // eslint-disable-next-line no-console
      console.error(
        `API: ${apiName} ❌\n\nZod Errors:\n`,
        fieldErrors,
        '\nResponse:\n',
        data,
      );
    }
    //! Just for debugging
    else {
      // eslint-disable-next-line no-console
      console.log(`API: { ${apiName} } checked successfully ✅`);
    }
  }
}
