import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ProductModel } from '../../../../models/product.model';
import { NgIf } from '@angular/common';

@Component({
  selector: 'test-product-card',
  standalone: true,
  imports: [NgIf],
  templateUrl: './product-card.component.html',
  styleUrl: './product-card.component.scss',
})
export class ProductCardComponent {
  @Input({ required: true }) product!: ProductModel;

  @Output() clickEvent = new EventEmitter<number>();

  public clicked(num: number): void {
    this.clickEvent.emit(num);
  }
}
