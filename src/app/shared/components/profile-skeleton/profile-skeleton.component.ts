import { SkeletonModule } from 'primeng/skeleton';

import { NgFor } from '@angular/common';
import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ContentCardComponent } from "../content-card/content-card.component";


@Component({
    selector: 'test-profile-skeleton',
    standalone: true,
    templateUrl: './profile-skeleton.component.html',
    styleUrl: './profile-skeleton.component.scss',
    changeDetection: ChangeDetectionStrategy.OnPush,
    imports: [SkeletonModule, NgFor, ContentCardComponent]
})
export class ProfileSkeletonComponent {}
