import { NgClass, NgFor } from '@angular/common';
import {
  Component,
  EventEmitter,
  Input,
  Output,
  WritableSignal,
  signal,
} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { InputTextModule } from 'primeng/inputtext';

@Component({
  selector: 'test-header',
  standalone: true,
  imports: [NgFor, NgClass, FormsModule, InputTextModule],
  templateUrl: './header.component.html',
  styleUrl: './header.component.scss',
})
export class HeaderComponent {
  @Input({ required: true }) categories!: Array<string>;
  @Input({ required: true }) itemsCount!: number;

  @Output() categoryEvent = new EventEmitter<string | null>();
  @Output() titleEvent = new EventEmitter<string | null>();

  activeCategory: WritableSignal<string | null> = signal(null);
  title: string | null = null;

  public selectCategory(category: string): void {
    this.activeCategory.set(
      this.activeCategory() === category ? null : category,
    );
    this.categoryEvent.emit(this.activeCategory());
  }

  public titleChanged(): void {
    this.titleEvent.emit(this.title);
  }
}
