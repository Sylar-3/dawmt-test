import { NgClass, NgIf } from '@angular/common';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';

@Component({
  selector: 'test-content-card',
  templateUrl: './content-card.component.html',
  styleUrl: './content-card.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  imports: [NgIf, NgClass],
})
export class ContentCardComponent {
  @Input() cardTitle: string | null = null;
  @Input() hasShadow: boolean = true;
  @Input() noPaddingBottom: boolean = false;
  @Input() loadingError: boolean = false;
  @Input() topExtension: boolean = false;
  @Input() bottomCard: boolean = false;

  @Output() reloadEvent = new EventEmitter();

  public reload(): void {
    this.reloadEvent.emit();
  }
}
