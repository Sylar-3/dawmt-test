import { SubSink } from 'subsink';

import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';

/**
 * A class that automatically unsubscribes all observables when
 * the object gets destroyed
 *
 * @export
 * @class UnsubscribeOnDestroyAdapter
 * @implements {OnDestroy}
 */
@Component({
  template: '',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
// eslint-disable-next-line @angular-eslint/component-class-suffix
export class UnsubscribeOnDestroyAdapter implements OnDestroy {
  /** The subscription sink object that stores all subscriptions
   * @memberof `UnsubscribeOnDestroyAdapter`
   */
  subs = new SubSink();

  /**
   * The lifecycle hook that unsubscribes all subscriptions
   * when the component / object gets destroyed
   */
  ngOnDestroy(): void {
    this.subs.unsubscribe();
  }
}
