import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  WritableSignal,
  signal,
} from '@angular/core';
import { UnsubscribeOnDestroyAdapter } from '../shared/unsubscribe-on-destroy-adapter';
import { BehaviorSubject, switchMap, tap, catchError, of, take } from 'rxjs';
import { ProductModel } from '../../models/product.model';
import { ProductsService } from '../shared/services/products.service';
import { ProductCardComponent } from '../shared/components/product-card/product-card.component';
import { NgFor, NgIf } from '@angular/common';
import { FooterComponent } from '../shared/components/footer/footer.component';
import { HeaderComponent } from '../shared/components/header/header.component';
import { ProfileSkeletonComponent } from '../shared/components/profile-skeleton/profile-skeleton.component';

@Component({
  selector: 'test-content',
  templateUrl: './content.component.html',
  styleUrl: './content.component.scss',
  changeDetection: ChangeDetectionStrategy.OnPush,
  standalone: true,
  providers: [ProductsService],
  imports: [
    ProductCardComponent,
    NgFor,
    FooterComponent,
    HeaderComponent,
    NgIf,
    ProfileSkeletonComponent,
  ],
})
export class ContentComponent
  extends UnsubscribeOnDestroyAdapter
  implements OnInit
{
  private productsPagination$ = new BehaviorSubject<string | null>(null);

  productsList: WritableSignal<Array<ProductModel>> = signal([]);
  filteredProductsList: WritableSignal<Array<ProductModel>> = signal([]);
  categoriesList: WritableSignal<Array<string>> = signal([]);
  productsCount: WritableSignal<number> = signal(0);

  loading: WritableSignal<boolean> = signal(true);

  constructor(private readonly _productService: ProductsService) {
    super();
  }

  ngOnInit(): void {
    this.getCategoriesList();
    this.createProductsSubscription();
  }

  private getCategoriesList(): void {
    this.subs.sink = this._productService
      .getCategoriesList()
      .pipe(
        tap((categories) => {
          this.categoriesList.set(categories);
        }),
        take(1),
        catchError((err) => {
          console.error(err);
          return of();
        }),
      )
      .subscribe();
  }

  private createProductsSubscription(): void {
    this.subs.sink = this.productsPagination$
      .pipe(
        switchMap((pagination) => {
          this.loading.set(true);
          return this._productService.getProductsList(pagination).pipe(
            tap((products) => {
              this.productsList.set(products);
              this.filteredProductsList.set(products);
              this.loading.set(false);
            }),
            catchError((err) => {
              this.loading.set(false);
              console.error(err);
              return of();
            }),
          );
        }),
      )
      .subscribe();
  }

  public changeCount(num: number): void {
    if (num === -1 && this.productsCount() === 0) {
      return;
    }

    this.productsCount.set(this.productsCount() + num);
  }

  public categoryChange(category: string | null): void {
    this.productsPagination$.next(category);
  }

  public searchForTitle(title: string | null): void {
    if (title && title.length > 0) {
      this.filteredProductsList.set(
        this.productsList().filter((product) =>
          product.name.toLocaleLowerCase().includes(title.toLocaleLowerCase()),
        ),
      );
    } else {
      this.filteredProductsList.set(this.productsList());
    }
  }
}
