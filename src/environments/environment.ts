/* eslint-disable @typescript-eslint/typedef */
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const DOMAIN = {
  test: 'https://fakestoreapi.com',
};

export const environment = {
  type: 'local',
  production: false,
  CURRENT_DOMAIN: DOMAIN.test,
};
