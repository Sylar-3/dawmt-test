/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable @typescript-eslint/typedef */
import { z } from 'zod';

export const ProductEntity = z.strictObject({
  id: z.number(),
  title: z.string(),
  price: z.number().min(0),
  description: z.string(),
  category: z.string(), //Should be an Enum
  image: z.string(),
  rating: z.strictObject({
    rate: z.number(),
    count: z.number().int().min(0),
  }),
});
export type ProductEntity = z.infer<typeof ProductEntity>;

export const ProductListEntity = ProductEntity.array();
export type ProductListEntity = z.infer<typeof ProductListEntity>;
