/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable @typescript-eslint/typedef */
import { z } from 'zod';

export const CategoriesEntity = z.string().array();
export type CategoriesEntity = z.infer<typeof CategoriesEntity>;
