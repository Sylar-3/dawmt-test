# Entities READ ME

Entity files are created with Zod objects
they are used to validate the response type of the backend
the core concept is to create an Entity for every API that we care about the integrity of its response

## Example 1
Listing API that returns
{
  data: Array<Type>
  total_count: number
}
we need to validate the content of the Array<Type>
to help us expect the errors from the backend and fix them early

## Example 2
Change Status API
since this that only sends the new status and only expects a response of type
{
  message: "Change successful"
}
we don't create an Entity for it because we only need the response code to be 200 or 201 
